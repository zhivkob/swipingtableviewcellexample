//
//  main.m
//  SwipingTableViewCellExample
//
//  Created by Zhivko Bogdanov on 1/15/14.
//  Copyright (c) 2014 Zhivko Bogdanov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MLNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MLNAppDelegate class]));
    }
}
