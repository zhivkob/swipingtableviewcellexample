//
//  SMLConstants.m
//  SmartLights
//
//  Created by Zhivko Bogdanov on 4/14/13.
//  Copyright (c) 2013 Zhivko Bogdanov. All rights reserved.
//

#import "SMLConstants.h"

//
// Data source constants
//
NSString *const kScreenLightKey = @"kFrontLight";
NSString *const kBackLightKey = @"kBackLight";

NSString *const kBellRingKey = @"kRingRingRing";
NSString *const kHooterKey = @"kHooter";
NSString *const kVanHornKey = @"kVanHorn";

NSString *const kSelectedLightsKey = @"kSelectedLights";
NSString *const kSelectedSignalKey = @"kSelectedSignal";

//
// Bounce animation constants
//
NSInteger const kBounceAnimationBouncesCount = 6;
CGFloat const kBounceAnimationRevealHorizontalOffset = 0.35f;
CGFloat const kBounceAnimationRevealVerticalOffset = 0.2f;
CGFloat const kBounceAnimationDuration = 0.25f;

CGFloat const kAutomaticSwipeAnimationDuration = 1.25f;

CGFloat const kRoundedCornerSize = 6.0f;
CGFloat const kCellRowHeight = 65.0f;

CGFloat const kCellDisplayAnimation = 0.8f;