//
//  SMLSettingsTableViewCell.m
//  SmartLights
//
//  Created by Zhivko Bogdanov on 4/14/13.
//  Copyright (c) 2013 Zhivko Bogdanov. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "SMLSwipeTableViewCell.h"
#import "SMLFadingImageView.h"
#import "CAAnimationGroup+Bounce.h"

@interface SMLSwipeTableViewCell()
{
    // The image view on the left
    SMLFadingImageView *_frontIconImageView;
    SMLFadingImageView *_backIconImageView;
    
    // The label containg the text in the cell
    UILabel *_cellTextLabel;
    
    // The Custom background view containing the rest view
    UIView *_overlayBackgroundView;
    
    // The initial touch point when the gesture begins
    CGPoint _initialTouch;
    
    // The offset on the left of the image behind the cell
    CGFloat _iconImageBackOffOffset;
}

@end

@implementation SMLSwipeTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self _init];
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self _init];
}

- (void)_init
{
    _backOffAnimationDuration = 0.45f;
    _effectsAnimationDuration = 0.2f;
    _allowedDistanceTolerance = 10.0f;
    _maximumDraggingDistance = 0.5f * self.frame.size.width;
    _borderDistance = 15.0f;
    _transformDistance = 66.0f;
    _swipeSlowFactor = 1.0f;
    _iconImageBackOffOffset = 11.0f;
    _allowedVerticalGestureOffset = 30.0f;
    _iconImageOffset = 19.0f;
    _textLabelOffset = 50.0f;
    _textLabelFont = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
    _cornerRadius = 7.0f;
    _acceptableHorizontalGestureOffset = 300.0f;
    _activeColor = [UIColor colorWithRed:0.32f green:0.64f blue:0.31f alpha:1.0f];
    _inactiveColor = [UIColor colorWithRed:0.54f green:0.62f blue:0.65f alpha:1.0f];
    _activeToInactiveColor = [UIColor colorWithRed:0.57f green:0.35f blue:0.46f alpha:1.0f];
    self.backgroundColor = [UIColor colorWithRed:0.93f green:0.93f blue:0.93f alpha:1.0f];
    
    // Replace the selected background view with an invisible one
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
    v.backgroundColor = [UIColor clearColor];
    self.selectedBackgroundView = v;
    
    _overlayBackgroundView = [[UIView alloc] init];
    _overlayBackgroundView.backgroundColor = _inactiveColor;
    [self.contentView addSubview:_overlayBackgroundView];
    
    // Setup the image view on the left
    _frontIconImageView = [[SMLFadingImageView alloc] init];
    _frontIconImageView.backgroundColor = [UIColor clearColor];
    _frontIconImageView.contentMode = UIViewContentModeLeft;
    [_overlayBackgroundView addSubview:_frontIconImageView];
    
    // Setup the image view on the left
    _backIconImageView = [[SMLFadingImageView alloc] init];
    _backIconImageView.backgroundColor = [UIColor clearColor];
    _backIconImageView.contentMode = UIViewContentModeRight;
    [self.contentView addSubview:_backIconImageView];
    
    // Setup the label containing the text
    _cellTextLabel = [[UILabel alloc] init];
    _cellTextLabel.backgroundColor = [UIColor clearColor];
    _cellTextLabel.font = _textLabelFont;
    _cellTextLabel.textColor = [UIColor whiteColor];
    [_overlayBackgroundView addSubview:_cellTextLabel];
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecognized:)];
    panGestureRecognizer.delegate = self;
    [self addGestureRecognizer:panGestureRecognizer];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognized:)];
    [self addGestureRecognizer:tapGestureRecognizer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect bounds = self.bounds;
    CGSize imageSize = _backIconImage.size.width > _frontIconImage.size.width ? _backIconImage.size : _frontIconImage.size;

    _overlayBackgroundView.frame = bounds;
    _backIconImageView.frame = CGRectMake(-imageSize.width, (bounds.size.height - imageSize.height) / 2.0f, imageSize.width, imageSize.height);
    _frontIconImageView.frame = CGRectMake(_iconImageOffset, (bounds.size.height - imageSize.height) / 2.0f, imageSize.width, imageSize.height);
    _cellTextLabel.frame = CGRectMake(_textLabelOffset, 0.0f, bounds.size.width - _textLabelOffset, bounds.size.height);
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // Accept only movements that are past given horizontal velocity
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        UIPanGestureRecognizer *panGestureRecognizer = (UIPanGestureRecognizer *)gestureRecognizer;
        
        if ([panGestureRecognizer velocityInView:self].x < -_acceptableHorizontalGestureOffset || [panGestureRecognizer velocityInView:self].x > _acceptableHorizontalGestureOffset)
            return YES;
        if ([panGestureRecognizer velocityInView:self].y < -_allowedVerticalGestureOffset || [panGestureRecognizer velocityInView:self].y > _allowedVerticalGestureOffset)
            return NO;
    }
    return YES;
}

- (void)toggleTableScrolling:(BOOL)canScroll
{
    [[self tableView] setScrollEnabled:canScroll];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:NO animated:animated];
}

- (void)setTextLabelText:(NSString *)textLabelText
{
    _textLabelText = textLabelText;
    _cellTextLabel.text = textLabelText;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    if (selected) {
        _overlayBackgroundView.backgroundColor = _activeColor;
    }
    
    // Animate the background color
    [UIView animateWithDuration:_backOffAnimationDuration
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         // Restore the overlay background view
                         if (!selected) {
                             _overlayBackgroundView.backgroundColor = _inactiveColor;
                         }
                         _overlayBackgroundView.transform = CGAffineTransformIdentity;
                         _backIconImageView.transform = CGAffineTransformIdentity;
                         _backIconImageView.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         // Change the sign behind the cell (cross or tick)
                         _backIconImageView.image = selected ? _backIconImageSelected : _backIconImage;
                         _backIconImageView.alpha = 1.0f;
                         
                         if ([self.delegate respondsToSelector:@selector(tableView:swipeTableViewCellDidChangeSelectedState:)]) {
                             [self.delegate tableView:[self tableView] swipeTableViewCellDidChangeSelectedState:self];
                         }
                     }];
    
    [UIView animateWithDuration:_effectsAnimationDuration
                          delay:0.5f * _backOffAnimationDuration
                        options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         _frontIconImageView.alpha = 1.0f;
                     }
                     completion:NULL];
    
    [_frontIconImageView setImage:(selected ? _frontIconImageSelected : _frontIconImage) animated:YES duration:_effectsAnimationDuration];
    [self setCornerRadius:0.0f toLayer:_overlayBackgroundView.layer];
}

- (UIColor *)colorFromInterpolatingUIColor:(UIColor *)color1 andUIColor:(UIColor *)color2 withValue:(CGFloat)value
{
    // Eliminate values outside of 0, 1
    value = MIN(MAX(0, value), 1);
    
    // Grab color components
    const CGFloat *firstColorComponents = CGColorGetComponents(color1.CGColor);
    const CGFloat *secondColorComponents = CGColorGetComponents(color2.CGColor);
    
    // Interpolate between colors
    CGFloat interpolatedComponents[CGColorGetNumberOfComponents(color1.CGColor)];
    for (NSUInteger i = 0; i < CGColorGetNumberOfComponents(color1.CGColor); i++) {
        interpolatedComponents[i] = firstColorComponents[i] * (1 - value) + secondColorComponents[i] * value;
    }
    
    // Create interpolated color
    CGColorRef interpolatedCGColor = CGColorCreate(CGColorGetColorSpace(color1.CGColor), interpolatedComponents);
    UIColor *interpolatedColor = [UIColor colorWithCGColor:interpolatedCGColor];
    CGColorRelease(interpolatedCGColor);
    
    return interpolatedColor;
}

- (UITableView *)tableView
{
    if (floorf(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        return (UITableView *)self.superview.superview;
    }
    else {
        return (UITableView *)self.superview;
    }
}

- (void)swipeRecognized:(UIPanGestureRecognizer *)gr
{
    // Get the parent table view and the index path of the current cell
    UITableView *tableView = [self tableView];
    BOOL selected = self.isSelected;
    
    // Don't allow the selection of already selected cells
    if (self.isSelected && !tableView.allowsMultipleSelection) {
        return;
    }
    
    if (gr.state == UIGestureRecognizerStateBegan) {
        // Animate the corner radius change
        [self setCornerRadius:_cornerRadius toLayer:_overlayBackgroundView.layer];
        
        // Disable scrolling
        [self toggleTableScrolling:NO];
        _initialTouch = [gr locationInView:self];
    }
    else if (gr.state == UIGestureRecognizerStateChanged) {
        // Calculate the distance traveled with the finger
        // Create 2 interpolation values, one taking into account the maximum dragging distance and one the acceptable distance
        CGPoint nextTouch = [gr locationInView:self];
        CGFloat distance = (nextTouch.x - _initialTouch.x) / _swipeSlowFactor;
        CGFloat smallAlphaInterpolation = fabsf(distance) / _transformDistance;
        CGFloat bigAlphaInterpolation = fabsf(distance) / _maximumDraggingDistance;
        CGFloat translationInterpolation = fabsf(distance) / _borderDistance;
        
        // If we are going left of the bounds of the view or going right of the maximum allowed distance
        // then don't execute any further
        if (distance < 0.0f || distance >= _maximumDraggingDistance) {
            return;
        }
        
        // Interpolate the background color
        _overlayBackgroundView.backgroundColor = selected ? [self colorFromInterpolatingUIColor:_activeColor andUIColor:_activeToInactiveColor withValue:smallAlphaInterpolation] : [self colorFromInterpolatingUIColor:_inactiveColor andUIColor:_activeColor withValue:smallAlphaInterpolation];

        // Fade in the sign image view
        _frontIconImageView.alpha = smallAlphaInterpolation;
        
        // Move the views
        _swipeSlowFactor = 1.0f + bigAlphaInterpolation * 1.25f;
        _frontIconImageView.alpha = 1.0f - smallAlphaInterpolation;
        _overlayBackgroundView.transform = CGAffineTransformMakeTranslation(distance, 0.0f);
        
        // Translate the sign image view a little when the interpolation value is between 0 and 1 and bind it to the edge of the bigger content view
        // if it exceeds the bounds
        if (translationInterpolation >= 0.0f && translationInterpolation <= 1.0f) {
            _backIconImageView.transform = CGAffineTransformMakeTranslation((translationInterpolation - 1.0f) * _iconImageBackOffOffset, 0.0f);
        }
        else {
            _backIconImageView.transform = CGAffineTransformMakeTranslation(distance - _borderDistance, 0.0f);
        }
    }
    else if (gr.state == UIGestureRecognizerStateEnded) {
        // Calculate the distance traveled with the finger
        CGPoint nextTouch = [gr locationInView:self];
        CGFloat distance = (nextTouch.x - _initialTouch.x) / _swipeSlowFactor;
        
        // If the distance was enough and we released our finger
        if (distance > _transformDistance - _allowedDistanceTolerance) {
            // Get the index path of the cell
            NSIndexPath *indexPath = [tableView indexPathForCell:self];
            _overlayBackgroundView.backgroundColor = selected ? _activeColor : _activeToInactiveColor;
            
            if ([self.delegate respondsToSelector:@selector(tableView:swipeTableViewCellWillChangeSelectedState:)]) {
                [self.delegate tableView:tableView swipeTableViewCellWillChangeSelectedState:self];
            }
            
            // Select / Deselect the cell
            if (!selected) {
                [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            else {
                if (tableView.allowsMultipleSelection) {
                    [tableView deselectRowAtIndexPath:indexPath animated:NO];
                }
                else {
                    // Restore the position and the colors of the view
                    [self snapBack];
                }
            }
        }
        else {
            // Restore the position and the colors of the view
            [self snapBack];
        }
        
        // Restore the scrolling of the view
        [self toggleTableScrolling:YES];
    }
}

- (void)snapBack
{
    if ([self.delegate respondsToSelector:@selector(tableView:swipeTableViewCellWillSnapBackToInitialState:)]) {
        [self.delegate tableView:[self tableView] swipeTableViewCellWillSnapBackToInitialState:self];
    }
    
    [UIView animateWithDuration:_backOffAnimationDuration
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         // Restore the position
                         _overlayBackgroundView.transform = CGAffineTransformIdentity;
                         _overlayBackgroundView.backgroundColor = self.isSelected ? _activeColor : _inactiveColor;
                         _frontIconImageView.transform = CGAffineTransformIdentity;
                         _frontIconImageView.alpha = 1.0f;
                         _backIconImageView.transform = CGAffineTransformIdentity;
                         _backIconImageView.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         _backIconImageView.alpha = 1.0f;
                         
                         if ([self.delegate respondsToSelector:@selector(tableView:swipeTableViewCellDidSnapBackToInitialState:)]) {
                             [self.delegate tableView:[self tableView] swipeTableViewCellDidSnapBackToInitialState:self];
                         }
                     }];
    
    [self setCornerRadius:0.0f toLayer:_overlayBackgroundView.layer];
}

- (void)tapRecognized:(UITapGestureRecognizer *)gr
{
    // Don't allow a selected sound cell to play the animation
    if (self.isSelected) {
        // Get the parent table view and the index path of the current cell
        UITableView *tableView = [self tableView];
        
        if (!tableView.allowsMultipleSelection) {
            return;
        }
    }
    
    CAAnimationGroup *animation = [CAAnimationGroup bounceView:self.contentView
                                                 withDirection:ZGBBounceDirectionRight
                                                      duration:0.15f
                                                        offset:0.08f
                                                     withValue:@"animation"
                                                        forKey:@"id"
                                                   bounceCount:4];
    animation.delegate = self;
    [_overlayBackgroundView.layer addAnimation:animation forKey:@"bounceAnimation"];
    [self setCornerRadius:_cornerRadius toLayer:_overlayBackgroundView.layer];
    
    _backIconImageView.hidden = YES;
    self.userInteractionEnabled = NO;
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [self setCornerRadius:0.0f toLayer:_overlayBackgroundView.layer];
    _backIconImageView.hidden = NO;
    self.userInteractionEnabled = YES;
}

- (void)setCornerRadius:(CGFloat)radius toLayer:(CALayer *)layer
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animation.fromValue = @(_overlayBackgroundView.layer.cornerRadius);
    animation.toValue = @(radius);
    animation.duration = _effectsAnimationDuration;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    layer.cornerRadius = radius;
    [layer addAnimation:animation forKey:@"cornerRadius"];
}

@end
