//
//  SMLFadingImageView.h
//  Visible
//
//  Created by Zhivko Bogdanov on 4/17/13.
//  Copyright (c) 2013 Zhivko Bogdanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMLFadingImageView : UIImageView

- (void)setImage:(UIImage *)image animated:(BOOL)animated duration:(CGFloat)duration;

@end
