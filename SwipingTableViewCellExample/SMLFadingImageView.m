//
//  SMLFadingImageView.m
//  Visible
//
//  Created by Zhivko Bogdanov on 4/17/13.
//  Copyright (c) 2013 Zhivko Bogdanov. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SMLFadingImageView.h"

@implementation SMLFadingImageView

- (void)setImage:(UIImage *)image animated:(BOOL)animated duration:(CGFloat)duration
{
    if ([UIImagePNGRepresentation(image) isEqualToData:UIImagePNGRepresentation(self.image)]) {
        return;
    }
    
    if (animated) {
        // Add a fading animation to the image to be set
        CATransition *animation = [CATransition animation];
        animation.duration = duration;
        animation.type = kCATransitionFade;
        [self.layer addAnimation:animation forKey:@"imageFade"];
    }
    
    [super setImage:image];
}

- (void)setHighlighted:(BOOL)highlighted
{
    if (self.highlighted == highlighted) {
        return;
    }
    
    // Add a fading animation to the image to be set
    CATransition *animation = [CATransition animation];
    animation.duration = 0.15f;
    animation.delegate = self;
    animation.type = kCATransitionFade;
    [self.layer addAnimation:animation forKey:@"imageFade"];
    
    [super setHighlighted:highlighted];
}

@end
