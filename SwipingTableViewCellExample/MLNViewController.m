//
//  MLNViewController.m
//  SwipingTableViewCellExample
//
//  Created by Zhivko Bogdanov on 1/15/14.
//  Copyright (c) 2014 Zhivko Bogdanov. All rights reserved.
//

#import "MLNViewController.h"

@interface MLNViewController ()
{
    AVAudioPlayer *_audioPlayer;
    __weak IBOutlet UITableView *_tableView;
}

@end

@implementation MLNViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SMLSwipeTableViewCell *cell = (SMLSwipeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabelText = @"Test cell";
    cell.delegate = self;
    cell.backIconImage = [UIImage imageNamed:@"CheckGreen"];
    cell.backIconImageSelected = [UIImage imageNamed:@"CrossRed"];
    cell.frontIconImage = [UIImage imageNamed:@"CellArrowRight"];
    cell.frontIconImageSelected = [UIImage imageNamed:@"CheckWhite"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView swipeTableViewCellWillChangeSelectedState:(SMLSwipeTableViewCell *)cell
{
    // Play the default sound if we are selecting a light
    if (tableView.allowsMultipleSelection) {
        double delayInSeconds = cell.effectsAnimationDuration;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            // Play the selected sound after 0.15 seconds
            NSError *error;
            NSURL *filepath = [[NSBundle mainBundle] URLForResource:@"Cellmark" withExtension:@"caf"];
            
            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:filepath error:&error];
            [_audioPlayer prepareToPlay];
            [_audioPlayer play];
        });
    }
    
    if (!cell.isSelected) {
        // If we are picking a sound
        if (!tableView.allowsMultipleSelection) {
            double delayInSeconds = cell.effectsAnimationDuration;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                // Play the selected sound after 0.15 seconds
                NSError *error;
                NSURL *filepath = [[NSBundle mainBundle] URLForResource:@"Hooter" withExtension:@"caf"];
                
                _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:filepath error:&error];
                [_audioPlayer prepareToPlay];
                [_audioPlayer play];
            });
        }
    }
}

@end
