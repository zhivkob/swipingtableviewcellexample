//
//  SMLConstants.h
//  SmartLights
//
//  Created by Zhivko Bogdanov on 4/14/13.
//  Copyright (c) 2013 Zhivko Bogdanov. All rights reserved.
//

#import <Foundation/Foundation.h>

//
// Data source constants
//
FOUNDATION_EXPORT NSString *const kScreenLightKey;
FOUNDATION_EXPORT NSString *const kBackLightKey;

FOUNDATION_EXPORT NSString *const kBellRingKey;
FOUNDATION_EXPORT NSString *const kHooterKey;
FOUNDATION_EXPORT NSString *const kVanHornKey;

FOUNDATION_EXPORT NSString *const kSelectedLightsKey;
FOUNDATION_EXPORT NSString *const kSelectedSignalKey;

//
// Animation constants
//
FOUNDATION_EXPORT NSInteger const kBounceAnimationBouncesCount;
FOUNDATION_EXPORT CGFloat const kBounceAnimationRevealHorizontalOffset;
FOUNDATION_EXPORT CGFloat const kBounceAnimationRevealVerticalOffset;
FOUNDATION_EXPORT CGFloat const kBounceAnimationDuration;

FOUNDATION_EXPORT CGFloat const kAutomaticSwipeAnimationDuration;

FOUNDATION_EXPORT CGFloat const kRoundedCornerSize;
FOUNDATION_EXPORT CGFloat const kCellRowHeight;

FOUNDATION_EXPORT CGFloat const kCellDisplayAnimation;