//
//  MLNAppDelegate.h
//  SwipingTableViewCellExample
//
//  Created by Zhivko Bogdanov on 1/15/14.
//  Copyright (c) 2014 Zhivko Bogdanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MLNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
