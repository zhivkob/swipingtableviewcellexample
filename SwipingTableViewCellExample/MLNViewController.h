//
//  MLNViewController.h
//  SwipingTableViewCellExample
//
//  Created by Zhivko Bogdanov on 1/15/14.
//  Copyright (c) 2014 Zhivko Bogdanov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SMLSwipeTableViewCell.h"

@interface MLNViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SMLSwipeTableViewCellDelegate>

@end
