//
//  SMLSettingsTableViewCell.h
//  SmartLights
//
//  Created by Zhivko Bogdanov on 4/14/13.
//  Copyright (c) 2013 Zhivko Bogdanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SMLSwipeTableViewCell;

@protocol SMLSwipeTableViewCellDelegate <NSObject>

@optional

/**
 *  This delegate method is called right before the selected state of the cell gets changed.
 *
 *  @param tableView The table view.
 *  @param cell      The swipe table view cell in the table view.
 */
- (void)tableView:(UITableView *)tableView swipeTableViewCellWillChangeSelectedState:(SMLSwipeTableViewCell *)cell;

/**
 *  This delegate method is called right after the selected state of the cell gets changed.
 *
 *  @param tableView The table view.
 *  @param cell      The swipe table view cell in the table view.
 */
- (void)tableView:(UITableView *)tableView swipeTableViewCellDidChangeSelectedState:(SMLSwipeTableViewCell *)cell;

/**
 *  This delegate method is called right before the cell state is cancelled and the cell gets moved back to its initial position.
 *
 *  @param tableView The table view.
 *  @param cell      The swipe table view cell in the table view.
 */
- (void)tableView:(UITableView *)tableView swipeTableViewCellWillSnapBackToInitialState:(SMLSwipeTableViewCell *)cell;

/**
 *  This delegate method is called right after the cell state is cancelled and the cell gets moved back to its initial position.
 *
 *  @param tableView The table view.
 *  @param cell      The swipe table view cell in the table view.
 */
- (void)tableView:(UITableView *)tableView swipeTableViewCellDidSnapBackToInitialState:(SMLSwipeTableViewCell *)cell;

@end

@interface SMLSwipeTableViewCell : UITableViewCell <UIGestureRecognizerDelegate>

// The delegate of the cell
@property (weak, nonatomic) id<SMLSwipeTableViewCellDelegate> delegate;

// A value which we use to slow down the swipe effect
@property (nonatomic) CGFloat swipeSlowFactor;

// This is the animation duration when we restore the view
@property (nonatomic) CGFloat backOffAnimationDuration;

// This is the animation duration after the first animation
@property (nonatomic) CGFloat effectsAnimationDuration;

// The distance we can maximally drag
@property (nonatomic) CGFloat maximumDraggingDistance;

// The distance needed to exceed before we invert the state of the cell
@property (nonatomic) CGFloat borderDistance;

// The distance where the cell gets fully transformed into the opposite state
@property (nonatomic) CGFloat transformDistance;

// The distance we can tolerate to select a cell
@property (nonatomic) CGFloat allowedDistanceTolerance;

// The horizontal distance we can tolerate to accept a movement as a horizontal movement
@property (nonatomic) CGFloat acceptableHorizontalGestureOffset;

// The vertical distance we can tolerate to accept a movement as a horizontal movement
@property (nonatomic) CGFloat allowedVerticalGestureOffset;

// The offset of the image in the cell
@property (nonatomic) CGFloat iconImageOffset;

// The offset of the text off of the image view in the cell
@property (nonatomic) CGFloat textLabelOffset;

// The font of the text label
@property (strong, nonatomic) UIFont *textLabelFont;

// The amount of rounding of the corners
@property (nonatomic) CGFloat cornerRadius;

// The label containing the text
@property (strong, nonatomic) NSString *textLabelText;

// The active color of the cell
@property (strong, nonatomic) UIColor *activeColor;

// The inactive color of the cell
@property (strong, nonatomic) UIColor *inactiveColor;

// The color that is used when changing from active to inactive state
@property (strong, nonatomic) UIColor *activeToInactiveColor;

// The image in the cell in non-selected state
@property (strong, nonatomic) UIImage *frontIconImage;

// The image in the cell in selected state
@property (strong, nonatomic) UIImage *frontIconImageSelected;

// The image outside the cell in non-selected state
@property (strong, nonatomic) UIImage *backIconImage;

// The image outside the cell in selected state
@property (strong, nonatomic) UIImage *backIconImageSelected;

@end
